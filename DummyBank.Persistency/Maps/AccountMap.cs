﻿using DummyBank.Domain;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace DummyBank.Persistency.Maps
{
    public class AccountMap : IEntityMap
    {
        public void Map(ModelBuilder builder)
        {
            builder.Entity<Account>()
                .HasKey(model => model.Id);

            builder.Entity<Account>()
                .Property(model => model.Id)
                .UsePropertyAccessMode(PropertyAccessMode.Field);

            builder.Entity<Account>()
                .Property(model => model.Name)
                .UsePropertyAccessMode(PropertyAccessMode.Field);

            builder.Entity<Account>()
                .Property(model => model.Deleted)
                .UsePropertyAccessMode(PropertyAccessMode.Field);

            builder.Entity<Account>()
                .Property(model => model.IsActive)
                .UsePropertyAccessMode(PropertyAccessMode.Field);

            builder.Entity<Account>()
                .Property(model => model.Type)
                .UsePropertyAccessMode(PropertyAccessMode.Field);

            builder.Entity<Account>()
                .Metadata.FindNavigation(nameof(Account.Transactions))
                .SetPropertyAccessMode(PropertyAccessMode.Field);
            
            builder.Entity<Account>()
                .Ignore(model => model.Balance);

            builder.Entity<Account>()
                .HasMany(model => model.Transactions)
                .WithOne(model => model.Account)
                .OnDelete(DeleteBehavior.Restrict);
        }

    }
}
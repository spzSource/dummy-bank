﻿using DummyBank.Domain;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace DummyBank.Persistency.Maps
{
    public class TransactionMap : IEntityMap
    {
        public void Map(ModelBuilder builder)
        {
            builder.Entity<Transaction>()
                .HasKey(model => model.Id);

            builder.Entity<Transaction>()
                .Property(model => model.Amount)
                .UsePropertyAccessMode(PropertyAccessMode.Field)
                .HasColumnType("Money");

            builder.Entity<Transaction>()
                .Property(model => model.Timestamp)
                .UsePropertyAccessMode(PropertyAccessMode.Field);

            builder.Entity<Transaction>()
                .Metadata.FindNavigation(nameof(Transaction.Account))
                .SetPropertyAccessMode(PropertyAccessMode.Field);
        }
    }
}
﻿using Microsoft.EntityFrameworkCore;

namespace DummyBank.Persistency.Maps
{
    /// <summary>
    /// Represents mapping between db table and domain object.
    /// </summary>
    interface IEntityMap
    {
        /// <summary>
        /// Setup configuration for specific model.
        /// </summary>
        /// <param name="builder"></param>
        void Map(ModelBuilder builder);
    }
}
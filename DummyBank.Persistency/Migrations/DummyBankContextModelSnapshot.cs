﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;

namespace DummyBank.Persistency.Migrations
{
    [DbContext(typeof(DummyBankContext))]
    partial class DummyBankContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("DummyBank.Domain.Account", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("PropertyAccessMode", PropertyAccessMode.Field);

                    b.Property<bool>("Deleted")
                        .HasAnnotation("PropertyAccessMode", PropertyAccessMode.Field);

                    b.Property<bool>("IsActive")
                        .HasAnnotation("PropertyAccessMode", PropertyAccessMode.Field);

                    b.Property<string>("Name")
                        .HasAnnotation("PropertyAccessMode", PropertyAccessMode.Field);

                    b.Property<int>("Type")
                        .HasAnnotation("PropertyAccessMode", PropertyAccessMode.Field);

                    b.HasKey("Id");

                    b.ToTable("Accounts");
                });

            modelBuilder.Entity("DummyBank.Domain.Transaction", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long?>("AccountId");

                    b.Property<decimal>("Amount")
                        .HasColumnType("Money")
                        .HasAnnotation("PropertyAccessMode", PropertyAccessMode.Field);

                    b.Property<DateTime>("Timestamp")
                        .HasAnnotation("PropertyAccessMode", PropertyAccessMode.Field);

                    b.HasKey("Id");

                    b.HasIndex("AccountId");

                    b.ToTable("Transactions");
                });

            modelBuilder.Entity("DummyBank.Domain.Transaction", b =>
                {
                    b.HasOne("DummyBank.Domain.Account", "Account")
                        .WithMany("Transactions")
                        .HasForeignKey("AccountId");
                });
        }
    }
}

﻿using DummyBank.Domain;

using Microsoft.EntityFrameworkCore.Migrations;

namespace DummyBank.Persistency.Migrations
{
    public partial class InitialTestData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            using (DummyBankContext context = new DummyBankContext())
            {
                Account watson = new Account("John Watson", AccountType.Saving).Debit(100m);
                Account hudson = new Account("Mrs. Hudson", AccountType.Saving).Debit(1000m);
                Account holmes = new Account("Sherlock Holmes", AccountType.Current).Debit(10m);

                context.Accounts.AddRange(
                    hudson,
                    watson,
                    holmes);

                context.SaveChanges();
            }
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

using DummyBank.Domain;
using DummyBank.Persistency.Maps;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace DummyBank.Persistency
{
    public class DummyBankContext : DbContext
    {
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Transaction> Transactions { get; set; }

        private readonly List<IEntityMap> _mappers =
            new List<IEntityMap>
            {
                new AccountMap(),
                new TransactionMap()
            };

        /// <summary>
        /// Required for seed-like migrations.
        /// </summary>
        public DummyBankContext()
        {
        }

        public DummyBankContext(DbContextOptions<DummyBankContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            _mappers.ForEach(map => map.Map(builder));
        }

        /// <summary>
        /// Required for seed-like migrations.
        /// </summary>
        /// <param name="optionsBuilder"></param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                string environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Development";

                IConfigurationRoot config = new ConfigurationBuilder()
                    .SetBasePath(Path.GetDirectoryName(GetType().GetTypeInfo().Assembly.Location))
                    .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                    .AddJsonFile($"appsettings.{environmentName}.json", optional: true)
                    .Build();

                optionsBuilder.UseSqlServer(config.GetConnectionString("DefaultConnection"));
            }
        }
    }
}
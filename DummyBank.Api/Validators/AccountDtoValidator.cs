﻿using DummyBank.Api.Models;

using FluentValidation;

namespace DummyBank.Api.Validators
{
    public class AccountDtoValidator : AbstractValidator<AccountDto>
    {
        public AccountDtoValidator()
        {
            RuleSet("Id", () =>
            {
                RuleFor(model => model.Id)
                    .NotEmpty()
                    .GreaterThan(0);
            });

            RuleSet("Basic", () =>
            {
                RuleFor(model => model.Name).NotNull();
            });
        }
    }
}
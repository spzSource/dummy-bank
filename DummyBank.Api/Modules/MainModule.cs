﻿using Autofac;

using DummyBank.Application.Commands;
using DummyBank.Application.Commands.Crud;
using DummyBank.Application.Queries;

namespace DummyBank.Api.Modules
{
    public class MainModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<AllAccountsQueryHandler>().AsImplementedInterfaces();
            builder.RegisterType<FindAccountQueryHandler>().AsImplementedInterfaces();
            builder.RegisterType<EditAccountCommandHandler>().AsImplementedInterfaces();
            builder.RegisterType<CreateAccountCommandHandler>().AsImplementedInterfaces();
            builder.RegisterType<DeleteAccountCommandHandler>().AsImplementedInterfaces();
            builder.RegisterType<FreezeAccountCommandHandler>().AsImplementedInterfaces();

            builder.RegisterType<AddInterestCommandHandler>().AsImplementedInterfaces();
            builder.RegisterType<DebitAccountCommandHandler>().AsImplementedInterfaces();
            builder.RegisterType<CreditAccountCommandHandler>().AsImplementedInterfaces();

            builder.RegisterType<TransferFundsCommandHandler>().AsImplementedInterfaces();
        }
    }
}
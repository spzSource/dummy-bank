﻿using System.Collections.Generic;

using Autofac;
using Autofac.Features.Variance;

using MediatR;

using Module = Autofac.Module;

namespace DummyBank.Api.Modules
{
    public class MediatorModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterSource(new ContravariantRegistrationSource());
            builder.RegisterType<Mediator>().As<IMediator>().InstancePerLifetimeScope();

            builder.Register<SingleInstanceFactory>(context => {
                IComponentContext componentContext = context.Resolve<IComponentContext>();
                return t =>
                {
                    object resolved;
                    return componentContext.TryResolve(t, out resolved) ? resolved : null;
                };
            }).InstancePerLifetimeScope();

            builder.Register<MultiInstanceFactory>(context => {
                IComponentContext componentContext = context.Resolve<IComponentContext>();
                return t => (IEnumerable<object>)componentContext.Resolve(typeof(IEnumerable<>).MakeGenericType(t));
            }).InstancePerLifetimeScope();
        }
    }
}
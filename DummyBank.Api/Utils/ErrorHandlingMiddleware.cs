﻿using System;
using System.Net;
using System.Threading.Tasks;

using DummyBank.Application;

using Microsoft.AspNetCore.Http;

using Newtonsoft.Json;

namespace DummyBank.Api.Utils
{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate _next;

        public ErrorHandlingMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            HttpStatusCode code = HttpStatusCode.InternalServerError;
            if (exception is DummyBankException)
            {
                code = HttpStatusCode.BadRequest;
            }
            string result = JsonConvert.SerializeObject(new { Error = exception.Message });

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)code;

            return context.Response.WriteAsync(result);
        }
    }
}
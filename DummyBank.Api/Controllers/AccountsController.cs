﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using DummyBank.Api.Models;
using DummyBank.Application.Commands;
using DummyBank.Application.Commands.Crud;
using DummyBank.Application.Queries;
using DummyBank.Domain;

using FluentValidation.AspNetCore;

using MediatR;

using Microsoft.AspNetCore.Mvc;

namespace DummyBank.Api.Controllers
{
    /// <summary>
    /// Represents entry point for accounts management.
    /// </summary>
    [Route("api/[controller]")]
    public class AccountsController : Controller
    {
        private readonly IMediator _mediator;

        /// <summary>
        /// .ctor for <see cref="AccountsController"/> class.
        /// </summary>
        public AccountsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Returns all available accounts (<see cref="Account"/>) except accounts which have already been marked as deleted.
        /// </summary>
        [HttpGet]
        public async Task<List<AccountDto>> Get()
        {
            List<Account> account = await _mediator.Send(new AllAccountsQuery());
            return account.Select(acc => new AccountDto(acc)).ToList();
        }

        /// <summary>
        /// Returns <see cref="Account"/> by specified identifier.
        /// </summary>
        /// <param name="id">Account identifier.</param>
        [HttpGet("{id}")]
        public async Task<AccountDto> Get(int id)
        {
            Account account = await _mediator.Send(new FindAccountQuery(id));
            return account != null ? new AccountDto(account) : null;
        }

        /// <summary>
        /// Creates a new <see cref="Account"/> using passed <see cref="AccountDto"/> model.
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> Create([CustomizeValidator(RuleSet = "Basic")][FromBody] AccountDto accountDto)
        {
            IActionResult actionResult;

            if (ModelState.IsValid && accountDto != null)
            {
                await _mediator.Send(new CreateAccountCommand(accountDto.Name, accountDto.Type));
                actionResult = Ok();
            }
            else
            {
                actionResult = BadRequest(ModelState);
            }

            return actionResult;
        }

        /// <summary>
        /// Updated <see cref="Account"/> using passed <see cref="AccountDto"/> model.
        /// </summary>
        [HttpPut]
        public async Task<IActionResult> Update([CustomizeValidator(RuleSet = "Id,Basic")][FromBody] AccountDto accountDto)
        {
            IActionResult actionResult;

            if (ModelState.IsValid && accountDto != null)
            {
                await _mediator.Send(new EditAccountCommand(accountDto.Id, accountDto.Name, accountDto.Type));
                actionResult = Ok();
            }
            else
            {
                actionResult = BadRequest(ModelState);
            }

            return actionResult;
        }

        /// <summary>
        /// Performs deletion for <see cref="Account"/> with specified identifier.
        /// </summary>
        /// <param name="id">Account identifier.</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public Task Delete(int id)
        {
            return _mediator.Send(new DeleteAccountCommand(id));
        }

        /// <summary>
        /// Performs account freezing.
        /// </summary>
        /// <param name="id">Account identifier.</param>
        /// <returns></returns>
        [HttpPost("{id}/freeze", Name = "FreezeAccount")]
        public Task Freeze(int id)
        {
            return _mediator.Send(new FreezeAccountCommand(id));
        }

        /// <summary>
        /// Performs debit operation for account with specified identifier.
        /// </summary>
        [HttpPost("{id}/debit/{amount}", Name = "DebitAccount")]
        public Task Debit(int id, decimal amount)
        {
            return _mediator.Send(new DebitAccountCommand(id, amount));
        }

        /// <summary>
        /// Performs credit operation for account with specified identifier.
        /// </summary>
        [HttpPost("{id}/credit/{amount}", Name = "CreditAccount")]
        public Task Credit(int id, decimal amount)
        {
            return _mediator.Send(new CreditAccountCommand(id, amount));
        }

        /// <summary>
        /// Applies annual interest for specified account.
        /// </summary>
        [HttpPost("{id}/annual-interest", Name = "AnnualInterest")]
        public Task AnnualInterest(int id)
        {
            return _mediator.Send(new AddInterestCommand(id));
        }

        /// <summary>
        /// Performs founds transfer between accounts.
        /// </summary>
        /// <param name="sourceId">Source account identifier.</param>
        /// <param name="targetId">Target account identifier.</param>
        /// <param name="amount">Amount for transfering.</param>
        /// <returns></returns>
        [HttpPost("{sourceId}/transfer/{targetId}/{amount}", Name = "TransferFunds")]
        public Task TransferFunds(int sourceId, int targetId, decimal amount)
        {
            return _mediator.Send(new TransferFundsCommand(amount, sourceId, targetId));
        }
    }
}

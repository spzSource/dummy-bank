﻿using System;
using System.Reflection;

using Autofac;
using Autofac.Extensions.DependencyInjection;

using DummyBank.Api.Modules;
using DummyBank.Api.Utils;
using DummyBank.Api.Validators;
using DummyBank.Persistency;

using FluentValidation.AspNetCore;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using Newtonsoft.Json;

namespace DummyBank.Api
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            Env = env;
            Configuration = builder.Build();
        }

        public IHostingEnvironment Env { get; }
        public IConfigurationRoot Configuration { get; }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<AccountDtoValidator>();

            if (Env.IsEnvironment("Testing"))
            {
                services.AddDbContext<DummyBankContext>(
                    options =>
                        options.UseInMemoryDatabase("TestDatabase"));
            }
            else
            {
                services.AddDbContext<DummyBankContext>(
                    options =>
                        options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            }
            
            services.AddMvc()
                .AddFluentValidation(e => e.RegisterValidatorsFromAssemblyContaining<Startup>())
                .AddJsonOptions(options => { options.SerializerSettings.Formatting = Formatting.Indented; });

            return new AutofacServiceProvider(ConfigureDependencies(services));
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseMiddleware<ErrorHandlingMiddleware>();
            app.UseMvc();
        }

        private IContainer ConfigureDependencies(IServiceCollection services)
        {
            ContainerBuilder builder = new ContainerBuilder();

            builder.Populate(services);

            /* Infrastructure modules */
            builder.RegisterModule<MainModule>();
            builder.RegisterModule<MediatorModule>();


            return builder.Build();
        }
    }
}

﻿using DummyBank.Domain;

namespace DummyBank.Api.Models
{
    public class AccountDto
    {
        /// <summary>
        /// Account identifier.
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Account name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Indicates whether account is deleted or not.
        /// </summary>
        public bool Deleted { get; set; }

        /// <summary>
        /// Indicates whether account is active or not.
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Account type.
        /// </summary>
        public AccountType Type { get; set; }

        /// <summary>
        /// Current account balance.
        /// </summary>
        public decimal Balance { get; set; }

        public AccountDto()
        {
            
        }

        public AccountDto(Account account)
        {
            Id = account.Id;
            Name = account.Name;
            Type = account.Type;
            Deleted = account.Deleted;
            Balance = account.Balance;
            IsActive = account.IsActive;
        }
    }
}
using System;

using DummyBank.Domain.Exceptions;
using DummyBank.Domain.Interest;

using Xunit;

namespace DummyBank.Domain.Tests
{
    public class AccountTests
    {
        [Fact]
        public void ShouldCreateActiveAccountByDefaultTest()
        {
            // configuration & execution
            Account account = new Account();
            
            // verification
            Assert.True(account.IsActive);
            Assert.False(account.Deleted);
        }

        [Fact]
        public void ShouldThrowExceptionWhenAccountNameIsEmptyTest()
        {
            // configuration & execution & verification
            Assert.Throws<ArgumentException>(() => new Account(String.Empty, 0.0m));
        }

        [Fact]
        public void ShouldNotPerformTransactionsIfAccountIsFreezedTest()
        {
            // configuration
            Account account = CreateTestAccount();

            // execution 
            account = account.Freeze();

            // verification
            Assert.Throws<AccountValidationException>(() => account.Debit(10m));
            Assert.Throws<AccountValidationException>(() => account.Credit(10m));
            Assert.Throws<AccountValidationException>(() => account.ApplyInterest(new SimpleInterest(1.1)));
        }

        [Fact]
        public void ShouldNotPerformTransactionsIfAccountIsDeletedTests()
        {
            // configuration
            Account account = CreateTestAccount();

            // execution 
            account = account.SoftDelete();

            // verification
            Assert.Throws<AccountValidationException>(() => account.Debit(10m));
            Assert.Throws<AccountValidationException>(() => account.Credit(10m));
            Assert.Throws<AccountValidationException>(() => account.ApplyInterest(new SimpleInterest(1.1)));
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-1000.8)]
        public void ShouldNotPerformDebitOrCreditIfAmountIsInvalidTest(decimal amount)
        {
            // configuration
            Account account = CreateTestAccount();

            // execution & verification
            Assert.Throws<AccountValidationException>(() => account.Debit(amount));
            Assert.Throws<AccountValidationException>(() => account.Credit(amount));
        }

        [Fact]
        public void ShouldDebitAndCreditBalanceTest()
        {
            // configuration
            Account account = CreateTestAccount();

            // execution
            account = account
                .Debit(15m)
                .Debit(15m)
                .Debit(25m)
                .Credit(5m);

            // verification
            Assert.Equal(50m, account.Balance);
        }

        [Fact]
        public void ShouldApplyInterestTest()
        {
            // configuration
            Account account = CreateTestAccount();

            // execution
            account = account
                .Debit(1000m)
                .ApplyInterest(new SimpleInterest(1.3));

            Assert.Equal(1013m, account.Balance);
        }

        private Account CreateTestAccount()
        {
            return new Account("Sherlock Holmes", AccountType.Current);
        }
    }
}

﻿using System.Net.Http;
using System.Threading.Tasks;

using DummyBank.Api.Models;

using Newtonsoft.Json;

namespace DummyBank.Api.Tests.Utils
{
    public static class HttpContentExtensions
    {
        public static async Task<T> ReadAsAsync<T>(this HttpContent content)
        {
            string result = await content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<T>(result);
        }
    }
}
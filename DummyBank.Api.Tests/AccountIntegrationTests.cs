﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using DummyBank.Api.Models;
using DummyBank.Api.Tests.Utils;
using DummyBank.Domain;
using DummyBank.Persistency;

using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;

using Xunit;
using Microsoft.Extensions.DependencyInjection;

using Newtonsoft.Json;

namespace DummyBank.Api.Tests
{
    public class AccountIntegrationTests : IDisposable
    {
        private readonly TestServer _server;
        private readonly HttpClient _client;

        public AccountIntegrationTests()
        {
            IWebHostBuilder builder = new WebHostBuilder()
                .UseEnvironment("Testing")
                .UseStartup<Startup>();

            _server = new TestServer(builder);
            _client = _server.CreateClient();

            BankContext.Database.EnsureDeleted();
            BankContext.Database.EnsureCreated();
        }

        public void Dispose()
        {
            BankContext.Database.EnsureDeleted();
        }

        private DummyBankContext BankContext => _server.Host.Services
            .GetRequiredService(typeof(DummyBankContext)) as DummyBankContext;

        [Fact]
        public async Task ShouldReturnOnlyActiveAccountsTest()
        {
            // configuration
            Account[] accounts = {
                new Account("John Watson", AccountType.Saving).Debit(100m),
                new Account("Mrs. Hudson", AccountType.Saving).Debit(1000m),
                new Account("Sherlock Holmes", AccountType.Current).Debit(10m),
                new Account("Irene Adler", AccountType.Current).Debit(200m).Freeze(),
                new Account("Mycroft Holmes", AccountType.Saving).Debit(10000m).SoftDelete()
            };

            await BankContext.Accounts.AddRangeAsync(accounts);
            await BankContext.SaveChangesAsync();

            // execution
            HttpResponseMessage result = await _client.GetAsync("/api/accounts");
            AccountDto[] retrievedAccounts = await result.Content.ReadAsAsync<AccountDto[]>();
            
            // verification
            Assert.Equal(4, retrievedAccounts.Length);
        }

        [Fact]
        public async Task ShouldReturnAccountByIdTest()
        {
            // configuration
            Account watson = new Account("John Watson", AccountType.Saving).Debit(100m);
            Account holmes = new Account("Mycroft Holmes", AccountType.Saving).Debit(10000m).SoftDelete();

            await BankContext.Accounts.AddRangeAsync(watson, holmes);
            await BankContext.SaveChangesAsync();

            // execution
            HttpResponseMessage result = await _client.GetAsync($"/api/accounts/{watson.Id}");
            AccountDto retrievedAccounts = await result.Content.ReadAsAsync<AccountDto>();
            
            // verification
            Assert.NotNull(retrievedAccounts);
            Assert.True(retrievedAccounts.IsActive);
            Assert.False(retrievedAccounts.Deleted);
            Assert.Equal(100m, retrievedAccounts.Balance);
            Assert.Equal("John Watson", retrievedAccounts.Name);
            Assert.Equal(AccountType.Saving, retrievedAccounts.Type);
        }

        [Fact]
        public async Task ShouldReturnNoContentIsAccountIsntExistsTest()
        {
            // configuration
            Account holmes = new Account("Mycroft Holmes", AccountType.Saving).Debit(10000m).SoftDelete();

            await BankContext.Accounts.AddAsync(holmes);
            await BankContext.SaveChangesAsync();

            // execution
            HttpResponseMessage result = await _client.GetAsync($"/api/accounts/{holmes.Id}");
            AccountDto retrievedAccounts = await result.Content.ReadAsAsync<AccountDto>();
            
            // verification
            Assert.Null(retrievedAccounts);
            Assert.Equal(HttpStatusCode.NoContent, result.StatusCode);
        }

        [Fact]
        public async Task ShouldCreateAccountTest()
        {
            // configuration & execution
            HttpResponseMessage result = await _client.PostAsync("/api/accounts", 
                new StringContent(JsonConvert.SerializeObject(new AccountDto
                {
                    Name = "Mycroft Holmes",
                    Deleted = false,
                    IsActive = true,
                    Type = AccountType.Current
                }), Encoding.UTF8, "application/json"));

            // verification
            Assert.Equal(1, BankContext.Accounts.Count());
            Assert.Equal(HttpStatusCode.OK, result.StatusCode);
        }

        [Fact]
        public async Task ShouldCheckForEmptyModelWhenCreateTest()
        {
            // configuration & execution
            HttpResponseMessage result = await _client.PostAsync("/api/accounts",
                new StringContent(String.Empty, Encoding.UTF8, "application/json"));

            // verification
            Assert.Equal(0, BankContext.Accounts.Count());
            Assert.Equal(HttpStatusCode.BadRequest, result.StatusCode);
        }

        [Fact]
        public async Task ShouldReturnBadRequestIfAccountIsNotFoundWhenDeleteTest()
        {
            // configuration & execution
            HttpResponseMessage result = await _client.DeleteAsync($"/api/accounts/{new Random(DateTime.Now.Millisecond).Next()}");

            // verification
            Assert.Equal(HttpStatusCode.BadRequest, result.StatusCode);
        }

        [Fact]
        public async Task ShouldCreditAccountTest()
        {
            // configuration 
            Account account = new Account("Mycroft Holmes", AccountType.Saving).Debit(10000m);

            await BankContext.Accounts.AddAsync(account);
            await BankContext.SaveChangesAsync();

            // execution
            HttpResponseMessage result = await _client.PostAsync($"/api/accounts/{account.Id}/credit/10000", new StreamContent(Stream.Null));
            Assert.Equal(HttpStatusCode.OK, result.StatusCode);

            result = await _client.GetAsync($"/api/accounts/{account.Id}");
            AccountDto retrievedAccounts = await result.Content.ReadAsAsync<AccountDto>();

            // verification
            Assert.Equal(0m, retrievedAccounts.Balance);
           
        }

        [Fact]
        public async Task ShouldDebitAccountTest()
        {
            // configuration 
            Account account = new Account("Mycroft Holmes", AccountType.Saving).Debit(10000m);

            await BankContext.Accounts.AddAsync(account);
            await BankContext.SaveChangesAsync();

            // execution
            HttpResponseMessage result = await _client.PostAsync($"/api/accounts/{account.Id}/debit/10000", new StreamContent(Stream.Null));
            Assert.Equal(HttpStatusCode.OK, result.StatusCode);

            result = await _client.GetAsync($"/api/accounts/{account.Id}");
            AccountDto retrievedAccounts = await result.Content.ReadAsAsync<AccountDto>();

            // verification
            Assert.Equal(20000m, retrievedAccounts.Balance);
            
        }

        [Theory]
        [InlineData(AccountType.Saving, 100, 101.3)]
        [InlineData(AccountType.Current, 100, 101.5)]
        public async Task ShouldAddAnnuamInterestTest(AccountType type, decimal initialBalance, decimal expectedBalance)
        {
            // configuration 
            Account account = new Account("Mycroft Holmes", type).Debit(initialBalance);

            await BankContext.Accounts.AddAsync(account);
            await BankContext.SaveChangesAsync();

            // execution
            HttpResponseMessage result = await _client.PostAsync($"/api/accounts/{account.Id}/annual-interest", new StringContent(String.Empty));
            Assert.Equal(HttpStatusCode.OK, result.StatusCode);

            result = await _client.GetAsync($"/api/accounts/{account.Id}");
            AccountDto retrievedAccounts = await result.Content.ReadAsAsync<AccountDto>();

            // verification
            Assert.Equal(expectedBalance, retrievedAccounts.Balance);
            
        }

        [Fact]
        public async Task ShouldTransferFundsTest()
        {
            // configuration 
            Account mycroft = new Account("Mycroft Holmes", AccountType.Current).Debit(1000m);
            Account sherlock = new Account("Sherlock Holmes", AccountType.Current).Debit(100m);

            await BankContext.Accounts.AddRangeAsync(mycroft, sherlock);
            await BankContext.SaveChangesAsync();

            // execution
            HttpResponseMessage result = await _client.PostAsync($"/api/accounts/{mycroft.Id}/transfer/{sherlock.Id}/{500}", new StringContent(String.Empty));
            Assert.Equal(HttpStatusCode.OK, result.StatusCode);

            // vefirication
            result = await _client.GetAsync($"/api/accounts/{mycroft.Id}");
            AccountDto mycroftDto = await result.Content.ReadAsAsync<AccountDto>();

            result = await _client.GetAsync($"/api/accounts/{sherlock.Id}");
            AccountDto sherlockDto = await result.Content.ReadAsAsync<AccountDto>();

            Assert.Equal(500m, mycroftDto.Balance);
            Assert.Equal(600m, sherlockDto.Balance);
        }
    }
}
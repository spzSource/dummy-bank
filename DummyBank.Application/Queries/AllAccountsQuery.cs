﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using DummyBank.Domain;
using DummyBank.Persistency;

using MediatR;

using Microsoft.EntityFrameworkCore;

namespace DummyBank.Application.Queries
{
    public class AllAccountsQuery : IRequest<List<Account>>
    {
    }

    public class AllAccountsQueryHandler : IAsyncRequestHandler<AllAccountsQuery, List<Account>>
    {
        private readonly DummyBankContext _context;

        public AllAccountsQueryHandler(DummyBankContext context)
        {
            _context = context;
        }

        public Task<List<Account>> Handle(AllAccountsQuery message)
        {
            return _context.Accounts.Where(acc => !acc.Deleted)
                .Include(acc => acc.Transactions)
                .ToListAsync();
        }
    }
}
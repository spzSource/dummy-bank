﻿using System.Threading.Tasks;

using DummyBank.Domain;
using DummyBank.Persistency;

using MediatR;

using Microsoft.EntityFrameworkCore;

namespace DummyBank.Application.Queries
{
    public class FindAccountQuery : IRequest<Account>
    {
        public long AccountId { get; }

        public FindAccountQuery(long accountId)
        {
            AccountId = accountId;
        }
    }

    public class FindAccountQueryHandler : IAsyncRequestHandler<FindAccountQuery, Account>
    {
        private readonly DummyBankContext _context;

        public FindAccountQueryHandler(DummyBankContext context)
        {
            _context = context;
        }

        public Task<Account> Handle(FindAccountQuery message)
        {
            return _context.Accounts
                .Include(acc => acc.Transactions)
                .SingleOrDefaultAsync(acc => acc.Id == message.AccountId && !acc.Deleted);
        }
    }
}
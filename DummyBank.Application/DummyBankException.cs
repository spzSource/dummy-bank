﻿using System;

namespace DummyBank.Application
{
    public abstract class DummyBankException : Exception
    {
        protected DummyBankException()
        {
        }

        protected DummyBankException(string message) : base(message)
        {
        }

        protected DummyBankException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
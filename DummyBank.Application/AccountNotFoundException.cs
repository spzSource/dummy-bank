﻿using System;

namespace DummyBank.Application
{
    public class AccountNotFoundException : DummyBankException
    {
        public AccountNotFoundException()
        {
        }

        public AccountNotFoundException(string message) : base(message)
        {
        }

        public AccountNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
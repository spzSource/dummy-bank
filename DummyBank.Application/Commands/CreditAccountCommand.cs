﻿using System.Threading.Tasks;

using DummyBank.Application.Queries;
using DummyBank.Domain;
using DummyBank.Persistency;

using MediatR;

using Microsoft.EntityFrameworkCore;

namespace DummyBank.Application.Commands
{
    public class CreditAccountCommand : IRequest
    {
        public long AccountId { get; }
        public decimal Amount { get; }

        public CreditAccountCommand(long accountId, decimal amount)
        {
            Amount = amount;
            AccountId = accountId;
        }
    }

    public class CreditAccountCommandHandler : IAsyncRequestHandler<CreditAccountCommand>
    {
        private readonly IMediator _mediator;
        private readonly DummyBankContext _context;

        public CreditAccountCommandHandler(DummyBankContext context, IMediator mediator)
        {
            _context = context;
            _mediator = mediator;
        }

        public async Task Handle(CreditAccountCommand message)
        {
            Account account = await _mediator.Send(new FindAccountQuery(message.AccountId));

            if (account != null)
            {
                Account updated = account.Credit(message.Amount);

                _context.Entry(account).State = EntityState.Detached;
                _context.Attach(updated).State = EntityState.Modified;

                await _context.SaveChangesAsync();
            }
            else
            {
                throw new AccountNotFoundException($"Cannot found any account by specified id = {message.AccountId})");
            }
        }
    }
}
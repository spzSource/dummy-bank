﻿using System.Collections.Generic;
using System.Threading.Tasks;

using DummyBank.Application.Queries;
using DummyBank.Domain;
using DummyBank.Domain.Interest;
using DummyBank.Persistency;

using MediatR;

using Microsoft.EntityFrameworkCore;

namespace DummyBank.Application.Commands
{
    public class AddInterestCommand : IRequest
    {
        public long AccountId { get; }

        public AddInterestCommand(long accountId)
        {
            AccountId = accountId;
        }
    }

    public class AddInterestCommandHandler : IAsyncRequestHandler<AddInterestCommand>
    {
        private readonly IMediator _mediator;
        private readonly DummyBankContext _context;

        private readonly IDictionary<AccountType, IInterest> _interestMap = 
            new Dictionary<AccountType, IInterest>
            {
                [AccountType.Saving] = new SimpleInterest(1.3),
                [AccountType.Current] = new SimpleInterest(1.5)
            };

        public AddInterestCommandHandler(DummyBankContext context, IMediator  mediator)
        {
            _context = context;
            _mediator = mediator;
        }

        public async Task Handle(AddInterestCommand message)
        {
            Account account = await _mediator.Send(new FindAccountQuery(message.AccountId));
            if (account != null)
            {
                IInterest interest;
                if (_interestMap.TryGetValue(account.Type, out interest))
                {
                    Account updated = account.ApplyInterest(interest);

                    _context.Entry(account).State = EntityState.Detached;
                    _context.Attach(updated).State = EntityState.Modified;

                    await _context.SaveChangesAsync();
                }
            }
            else
            {
                throw new AccountNotFoundException($"Cannot found any account by specified id = {message.AccountId})");
            }
        }
    }
}
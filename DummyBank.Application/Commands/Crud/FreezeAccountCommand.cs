﻿using System.Threading.Tasks;

using DummyBank.Application.Queries;
using DummyBank.Domain;
using DummyBank.Persistency;

using MediatR;

using Microsoft.EntityFrameworkCore;

namespace DummyBank.Application.Commands.Crud
{
    public class FreezeAccountCommand : IRequest
    {
        public long AccountId { get; }

        public FreezeAccountCommand(long accountId)
        {
            AccountId = accountId;
        }
    }

    public class FreezeAccountCommandHandler : IAsyncRequestHandler<FreezeAccountCommand>
    {
        private readonly IMediator _mediator;
        private readonly DummyBankContext _context;

        public FreezeAccountCommandHandler(DummyBankContext context, IMediator mediator)
        {
            _context = context;
            _mediator = mediator;
        }

        public async Task Handle(FreezeAccountCommand message)
        {
            Account account = await _mediator.Send(new FindAccountQuery(message.AccountId));

            if (account != null)
            {
                Account freezedAccount = account.Freeze();

                _context.Entry(account).State = EntityState.Detached;
                _context.Attach(freezedAccount).State = EntityState.Modified;

                await _context.SaveChangesAsync();
            }
            else
            {
                throw new AccountNotFoundException($"Cannot found any account by specified id = {message.AccountId})");
            }
        }
    }
}
﻿using System.Threading.Tasks;

using DummyBank.Application.Queries;
using DummyBank.Domain;
using DummyBank.Persistency;

using MediatR;

using Microsoft.EntityFrameworkCore;

namespace DummyBank.Application.Commands.Crud
{
    public class EditAccountCommand : IRequest
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public AccountType Type { get; set; }

        public EditAccountCommand(long id, string name, AccountType type)
        {
            Id = id;
            Name = name;
            Type = type;
        }
    }

    public class EditAccountCommandHandler : IAsyncRequestHandler<EditAccountCommand>
    {
        private readonly IMediator _mediator;
        private readonly DummyBankContext _context;

        public EditAccountCommandHandler(DummyBankContext context, IMediator mediator)
        {
            _context = context;
            _mediator = mediator;
        }

        public async Task Handle(EditAccountCommand message)
        {
            Account account = await _mediator.Send(new FindAccountQuery(message.Id));
            if (account != null)
            {
                Account updatedAccount = new Account(
                    account.Id, 
                    message.Name, 
                    account.Deleted, 
                    account.IsActive, 
                    message.Type,
                    account.Transactions);

                _context.Entry(account).State = EntityState.Detached;
                _context.Attach(updatedAccount).State = EntityState.Modified;

                await _context.SaveChangesAsync();
            }
            else
            {
                throw new AccountNotFoundException($"Cannot found any account by specified id = {message.Id})");
            }
        }
    }
}
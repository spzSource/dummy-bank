﻿using System.Threading.Tasks;

using DummyBank.Domain;
using DummyBank.Persistency;

using MediatR;

namespace DummyBank.Application.Commands.Crud
{
    public class CreateAccountCommand : IRequest
    {
        public string Name { get; }
        public AccountType Type { get; }

        public CreateAccountCommand(string name, AccountType type)
        {
            Name = name;
            Type = type;
        }
    }

    public class CreateAccountCommandHandler : IAsyncRequestHandler<CreateAccountCommand>
    {
        private readonly DummyBankContext _context;

        public CreateAccountCommandHandler(DummyBankContext context)
        {
            _context = context;
        }

        public Task Handle(CreateAccountCommand message)
        {
            Account account = new Account(message.Name, message.Type);

            _context.Accounts.Add(account);

            return _context.SaveChangesAsync();
        }
    }
}
﻿using System.Threading.Tasks;

using DummyBank.Application.Queries;
using DummyBank.Domain;
using DummyBank.Persistency;

using MediatR;

using Microsoft.EntityFrameworkCore;

namespace DummyBank.Application.Commands.Crud
{
    public class DeleteAccountCommand : IRequest
    {
        public long AccountId { get; }

        public DeleteAccountCommand(long accountId)
        {
            AccountId = accountId;
        }
    }

    public class DeleteAccountCommandHandler : IAsyncRequestHandler<DeleteAccountCommand>
    {
        private readonly IMediator _mediator;
        private readonly DummyBankContext _context;

        public DeleteAccountCommandHandler(DummyBankContext context, IMediator mediator)
        {
            _context = context;
            _mediator = mediator;
        }

        public async Task Handle(DeleteAccountCommand message)
        {
            Account accountToBeDeleted = await _mediator.Send(new FindAccountQuery(message.AccountId));

            if (accountToBeDeleted != null)
            {
                Account softDeletedAccount = accountToBeDeleted.SoftDelete();

                _context.Entry(accountToBeDeleted).State = EntityState.Detached;
                _context.Attach(softDeletedAccount).State = EntityState.Modified;

                await _context.SaveChangesAsync();
            }
            else
            {
                throw new AccountNotFoundException($"Cannot found any account by specified id = {message.AccountId})");
            }
        }
    }
}
﻿using System.Threading.Tasks;

using DummyBank.Application.Queries;
using DummyBank.Domain;
using DummyBank.Persistency;

using MediatR;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

namespace DummyBank.Application.Commands
{
    public class TransferFundsCommand : IRequest
    {
        public decimal Amount { get; }
        public long SourceAccountId { get; }
        public long TargetAccountId { get; }

        public TransferFundsCommand(decimal amount, long sourceAccountId, long targetAccountId)
        {
            Amount = amount;
            SourceAccountId = sourceAccountId;
            TargetAccountId = targetAccountId;
        }
    }

    public class TransferFundsCommandHandler : IAsyncRequestHandler<TransferFundsCommand>
    {
        private readonly IMediator _mediator;
        private readonly DummyBankContext _context;

        public TransferFundsCommandHandler(DummyBankContext context, IMediator mediator)
        {
            _context = context;
            _mediator = mediator;
        }

        public async Task Handle(TransferFundsCommand message)
        {
            Account sourceAccount = await _mediator.Send(new FindAccountQuery(message.SourceAccountId));
            Account targetAccount = await _mediator.Send(new FindAccountQuery(message.TargetAccountId));

            if (sourceAccount == null)
            {
                throw new AccountNotFoundException($"Cannot found any account by specified id = {message.SourceAccountId})");
            }

            if (targetAccount == null)
            {
                throw new AccountNotFoundException($"Cannot found any account by specified id = {message.TargetAccountId})");
            }

            Account updatedSourceAccount = sourceAccount.Credit(message.Amount);
            Account updatedTargetAccount = targetAccount.Debit(message.Amount);

            _context.Entry(sourceAccount).State = EntityState.Detached;
            _context.Entry(targetAccount).State = EntityState.Detached;

            _context.Attach(updatedSourceAccount).State = EntityState.Modified;
            _context.Attach(updatedTargetAccount).State = EntityState.Modified;

            await _context.SaveChangesAsync();
        }
    }
}
﻿using System;

namespace DummyBank.Domain
{
    public class Transaction
    {
        public long Id { get; private set; }
        public decimal Amount { get; private set; }
        public Account Account { get; private set; }
        public DateTime Timestamp { get; private set; }

        public Transaction()
        {
        }

        public Transaction(decimal amount, Account account)
        {
            Amount = amount;
            Account = account;
            Timestamp = DateTime.Now;
        }
    }
}
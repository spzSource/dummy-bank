﻿using System;
using System.Collections.Generic;
using System.Linq;

using DummyBank.Domain.Exceptions;
using DummyBank.Domain.Interest;

namespace DummyBank.Domain
{
    /// <summary>
    /// Represents bank account.
    /// </summary>
    public class Account
    {
        private readonly IList<Transaction> _transactions;

        /// <summary>
        /// Account identifier.
        /// </summary>
        public long Id { get; private set; }

        /// <summary>
        /// Account name.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Indicates whether account is deleted or not.
        /// </summary>
        public bool Deleted { get; private set; }

        /// <summary>
        /// Indicates whether account is active or not.
        /// </summary>
        public bool IsActive { get; private set; }

        /// <summary>
        /// Account type.
        /// </summary>
        public AccountType Type { get; private set; }

        /// <summary>
        /// Collection of all transactions for current account.
        /// </summary>
        public IEnumerable<Transaction> Transactions => _transactions.ToList();

        /// <summary>
        /// Current account balance.
        /// </summary>
        public decimal Balance => Transactions.Sum(transaction => transaction.Amount);

        /// <summary>
        /// Default .ctor for <see cref="Account"/>
        /// </summary>
        public Account()
        {
            Deleted = false;
            IsActive = true;
            _transactions = new List<Transaction>();
        }

        /// <summary>
        /// The .ctor for <see cref="Account"/> class.
        /// </summary>
        public Account(string name, AccountType type) : this()
        {
            if (String.IsNullOrEmpty(name))
            {
                throw new ArgumentException("Account must be with non empty name.", nameof(name));
            }

            Name = name;
            Type = type;
        }

        /// <summary>
        /// .ctor for Account copying.
        /// </summary>
        public Account(
            long id, 
            string name, 
            bool deleted, 
            bool isActive, 
            AccountType type,
            IEnumerable<Transaction> transactions)
        {
            Id = id;
            Name = name;
            Deleted = deleted;
            IsActive = isActive;
            Type = type;

            _transactions = transactions.ToList();
        }

        /// <summary>
        /// Freezes current account so that no transactions can take place on the account.
        /// </summary>
        public Account Freeze()
        {
            return new Account(Id, Name, Deleted, false, Type, Transactions);
        }

        /// <summary>
        /// Marks current account as deleted.
        /// </summary>
        public Account SoftDelete()
        {
            return new Account(Id, Name, true, IsActive, Type, Transactions);
        }

        /// <summary>
        /// Performs debit operation against current account.
        /// The result is a new <see cref="Transaction"/> to be persisted.
        /// </summary>
        public Account Debit(decimal amount)
        {
            ValidateActive();
            ValidateDeleted();
            ValidateAmount(amount);

            IList<Transaction> transactionsCopy = Transactions.ToList();
            transactionsCopy.Add(new Transaction(amount, this));

            return new Account(Id, Name, Deleted, IsActive, Type, transactionsCopy);
        }

        /// <summary>
        /// Performs credit operation against current account.
        /// The result is a new <see cref="Transaction"/> to be persisted.
        /// </summary>
        public Account Credit(decimal amount)
        {
            ValidateActive();
            ValidateDeleted();
            ValidateAmount(amount);

            IList<Transaction> transactionsCopy = Transactions.ToList();
            transactionsCopy.Add(new Transaction(Decimal.MinusOne * amount, this));

            return new Account(Id, Name, Deleted, IsActive, Type, transactionsCopy);
        }

        /// <summary>
        /// Performs changes against interest rate for current user.
        /// The interest rate must be in a range between 0 and 100 inclusively.
        /// </summary>
        public Account ApplyInterest(IInterest interest)
        {
            ValidateActive();
            ValidateDeleted();

            decimal amount = interest.Calculate(this);

            IList<Transaction> transactionsCopy = Transactions.ToList();
            transactionsCopy.Add(new Transaction(amount, this));

            return new Account(Id, Name, Deleted, IsActive, Type, transactionsCopy);
        }

        private void ValidateAmount(decimal value)
        {
            if (value <= 0 )
            {
                throw new AccountValidationException("There are invalid value to change current balance for current account. " +
                    "Please make sure that value is greater than zero and is in acceptable range.");
            }
        }

        private void ValidateActive()
        {
            if (!IsActive)
            {
                throw new AccountValidationException("The current account freezed. There is no ability to do any transactions against the account.");
            }
        }

        private void ValidateDeleted()
        {
            if (Deleted)
            {
                throw new AccountValidationException("The current account deleted.");
            }
        }
    }
}
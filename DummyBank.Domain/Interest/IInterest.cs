﻿namespace DummyBank.Domain.Interest
{
    public interface IInterest
    {
        decimal Calculate(Account account);
    }
}
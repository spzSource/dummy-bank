﻿namespace DummyBank.Domain.Interest
{
    public class SimpleInterest : IInterest
    {
        private readonly double _rate;

        public SimpleInterest(double rate)
        {
            _rate = rate;
        }

        public decimal Calculate(Account account)
        {
            return (decimal)_rate / 100 * account.Balance;
        }
    }
}
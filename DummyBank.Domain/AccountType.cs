﻿namespace DummyBank.Domain
{
    /// <summary>
    /// Represents an account type.
    /// </summary>
    public enum AccountType
    {
        Current,
        Saving
    }
}